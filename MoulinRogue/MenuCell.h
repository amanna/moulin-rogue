//
//  MenuCell.h
//  MoulinRogue
//
//  Created by Aditi on 28/06/16.
//  Copyright © 2016 Aditi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end
