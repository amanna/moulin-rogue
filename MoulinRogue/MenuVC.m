//
//  MenuVC.m
//  MoulinRogue
//
//  Created by Aditi on 28/06/16.
//  Copyright © 2016 Aditi. All rights reserved.
//

#import "MenuVC.h"
#import "MenuCell.h"
#import "AppDelegate.h"
@interface MenuVC (){
    AppDelegate *appDelegate;
}
@property(nonatomic)NSMutableArray *arrTitle;
@end

@implementation MenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
     appDelegate= (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.arrTitle = [[NSMutableArray alloc]initWithObjects:@"Agenda",@"Gallery",@"Video", @"Notify Me",@"Contact Us",appDelegate.strToken,nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrTitle.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(!cell){
        [tableView registerNib:[UINib nibWithNibName:@"MenuCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    }
    cell.lblTitle.text = [self.arrTitle objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *str = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    //eventCompletionHandler(str,kActionSelect);
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 71.0;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
