//
//  MenuCell.m
//  MoulinRogue
//
//  Created by Aditi on 28/06/16.
//  Copyright © 2016 Aditi. All rights reserved.
//

#import "MenuCell.h"

@implementation MenuCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
