//
//  MenuVC.h
//  MoulinRogue
//
//  Created by Aditi on 28/06/16.
//  Copyright © 2016 Aditi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@end
